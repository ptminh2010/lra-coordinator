# lra-coordinator Project
## Running the application in dev mode

#### Build & Install LRA

```shell script
mvn clean package
export IMAGE_TAG=$(git rev-parse --short HEAD)
docker build . -t registry.gitlab.com/my-public4/capstone-project/lra-coordinator:$IMAGE_TAG
docker login registry.gitlab.com
docker push registry.gitlab.com/my-public4/capstone-project/lra-coordinator:$IMAGE_TAG

#install
helm -n default upgrade --install --set image.tag=$IMAGE_TAG lra-coordinator-service helm/

```

AMQ Queue:
```shell script
docker pull registry.redhat.io/amq7/amq-broker-rhel8:7.11.0
docker tag registry.redhat.io/amq7/amq-broker-rhel8:7.11.0 registry.gitlab.com/microservice-banking/amq-broker-rhel8:7.11.0
docker push registry.gitlab.com/microservice-banking/amq-broker-rhel8:7.11.0
helm -n default upgrade --install amq-broker-rhel8 lra-coordinator/helm_amq/

```
#### Uninstall 
```shell script
#uninstall
helm uninstall -n default lra-coordinator-service
helm uninstall -n default amq-broker-rhel8

```